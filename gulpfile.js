const fs = require('fs');
const gulp = require("gulp");
const browserSync = require('browser-sync');
const proc = require('child_process');
var path = require("path");
var Builder = require('systemjs-builder');

gulp.task('systemBuild', async function () {

    var builder = new Builder('.', './system.config.js');

    return builder
    .bundle('./src/js/entry.js', './dist/js/app.js')
    .then(function() {
      console.log('Build complete');
    })
    .catch(function(err) {
      console.log('Build error');
      console.log(err);
    });

});

gulp.task("babelify_YUK", async function() {
    return gulp.src("src/js/**/*.js").pipe(babel({babelConfig})).pipe(gulp.dest("dist/js"));
});

gulp.task('cssify', async function() {
    return gulp.src("src/css/**/*.css").pipe(gulp.dest("dist/css"));
});

gulp.task('htmlify', async function() {
  return gulp.src(['src/*.html']).pipe(gulp.dest('dist'));
});

const shuntfunc = (from,to) => {
    const opts = {"allowEmpty": true};
    const fn = async () => {
    return gulp.src(from,opts).pipe(gulp.dest(to));
  };
  return fn;
};

gulp.task('shunt_vendor',         shuntfunc(['src/vendor/**'],  'dist/vendor'));
gulp.task('shunt_assets',         shuntfunc(['src/assets/**'],  'dist/assets'));
gulp.task('shunt_favicon',        shuntfunc(['src/favicon.ico'],'dist'));
gulp.task('shunt_fonts',          shuntfunc(['src/fonts/**'],   'dist/fonts'));
gulp.task('shunt_babel_polyfill', shuntfunc(['node_modules/babel-polyfill/dist/**'],    'dist/vendor/babel'));
gulp.task('shunt_systemjs',       shuntfunc(['node_modules/systemjs/dist/**'],          'dist/vendor/systemjs'));

gulp.task('shunt', gulp.parallel([
    'shunt_vendor',
    'shunt_assets',
    'shunt_favicon',
    'shunt_fonts',
    'shunt_babel_polyfill',
    'shunt_systemjs'
]));

gulp.task('run_server', async function() {
    browserSync({
        server: {
            open: false,
            browser: [],
            reloadOnRestart: true,
            baseDir: 'dist'
        },
        port: 8888,
        ui: false,
        ghostMode: false,
        logSnippet: false,
        open: false,
        notify: false
    });
    const watcherOptions = {
        debounceDelay: 3333,
        cwd: 'src'
    };
    var jsWatcher     = gulp.watch(['js/**/*.js'], watcherOptions, gulp.series(['systemBuild', browserSync.reload]));
    var cssWatcher    = gulp.watch(['css/**/*.css'], watcherOptions, gulp.series(['cssify', browserSync.reload]));
    var htmlWatcher   = gulp.watch(['**/*.html'], watcherOptions, gulp.series(['htmlify', browserSync.reload]));
    var vendorWatcher = gulp.watch(['vendor/**','assets/**','fonts/**'], watcherOptions, gulp.series(['shunt', browserSync.reload]));
    return gulp;
});

gulp.task('yay', () => {
    return new Promise((resolve,reject) => {
        let s = proc.spawn('/usr/bin/espeak',["the build ran"],{"detach": true});
        resolve(1);
    });
});

gulp.task('build', gulp.parallel(['shunt', 'systemBuild', 'cssify', 'htmlify']))

gulp.task('default', gulp.series(['build','yay']));

gulp.task('serve', gulp.series('build','run_server'));