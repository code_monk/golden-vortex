const multiply = (a, b = 2) => {
    return a * b;
}
export default multiply;